# util
import sys,os

# math
import numpy as np
from itertools import compress

# graph 
import networkx as nx

# optimization
import scipy.optimize

def get_nf_pwr2(s,N) :
   normfac=0
   for i in range(int(np.log2(N))-1):
      normfac+=2*(2**i)**s
   normfac+=(N/2)**s
   return normfac

def gen_all_gates_square(N,tmax) :
   gates=[]
   probs=[]

   for time in range(tmax) :
      for phase in [0,1] :
         i=0
         d=int(2**i)
         for site in range(0,d) :
            for site0 in range(int(site+phase*d),int(N),int(2*d)) :
               site1=int((site0+d)%N)
               gates.append((site0,site1))
               probs.append(1)

   if len(gates) != int(N*tmax) :
      return None,None
   return gates,probs

def gen_all_gates_pwr2(s,n,tmax=1) :
   N=int(2**n)
   normfac=get_nf_pwr2(s,N)

   gates=[]
   probs=[]
   for circuit_layers in range(tmax) : 
      for phase in [0,1] :
         for i in range(n-1) :
            d=2**i
            for site in range(0,d) :
               for site0 in range(site+phase*d,N,2*d) :
                  site1=int((site0+d)%N)
                  gates.append((site0,site1))
                  probs.append(d**s/normfac)

      i=n-1
      for site0 in range(int(N/2)):
         site1=int((site0+2**i)%N)
         gates.append((site0,site1))
         probs.append((2**i)**s/normfac)

   if len(gates)%(int(N*n-N/2)) != 0 :
      return None,None
   
   return gates,probs

def get_random_lattice(gates,probs,rng=np.random.default_rng(seed=19940527)) :
   randnums=rng.uniform(low=0,high=1,size=len(probs))
   applied_gates=list(compress(gates,list(map(lambda x,y: x<y,randnums,probs))))
   return applied_gates

class lattice:
   G=None
   gates=None
   edges=None
   nodes=None

   def make_lattice(self,N,gates,dx=1,dy=1,verbose=False,makenx=False) :
      # nodes and coordinates
      nodes=list(range(N))
      node_labels=[(i,i) for i in range(N)]
      coords=[(dx*i,0) for i in range(N)]

      numsites=len(nodes)
      edges=[]

      row=1
      applied_sites=list(range(N))
      numsites_row=0

      for gate in gates :
         if (gate[0] in applied_sites) and (gate[1] in applied_sites) :
            applied_sites.remove(gate[0])
            applied_sites.remove(gate[1])
            numsites_row+=1

         else :
            curr_dx=dx*(N-1)/(numsites_row+1)
            for i in range(1,numsites_row+1) :
               coords.append((curr_dx*i,dy*row))

            # reset 
            row+=1

            # do the above
            applied_sites=list(range(N))
            applied_sites.remove(gate[0])
            applied_sites.remove(gate[1])

            numsites_row=1

         curr_numedges=0
         nodes.append(len(nodes))
         node_labels.append(gate)
         
         a=gate[0]
         b=gate[1]

         for i in list(range(len(nodes)-1))[::-1]:
            found=int(a in node_labels[i]) + int(b in node_labels[i])
            if (0<found) :
               if (a in node_labels[i]) :
                  a=len(nodes)
               if (b in node_labels[i]) :
                  b=len(nodes)

               curr_edge=(nodes[-1],nodes[i])
               edges.append(curr_edge)
               curr_numedges+=found

               if 2<=curr_numedges :
                  break
         if curr_numedges != 2 :
            print('something is wrong')
            print(found)
            print(curr_edge)

      curr_dx=dx*(N-1)/(numsites_row+1)
      for i in range(1,numsites_row+1) :
         coords.append((curr_dx*i,dy*row))
      row+=1

      for boundarysite in range(N) :
         nodes.append(len(nodes))
         node_labels.append((boundarysite,boundarysite))
         curr_numedges=0

         for i in list(range(len(nodes)-1))[::-1]:
            found = int(boundarysite in node_labels[i]) or int(boundarysite in node_labels[i])
            if (0<found) :
               edges.append((nodes[-1],nodes[i]))
               curr_numedges+=found

               if 1<=curr_numedges :
                  break

      for i in range(N) :
         coords.append((dx*i,dy*row))

      # make sure world line is consistent.
      for i in range(N) :
         count=0
         for edge in edges :
            count+= int(i in edge)
      
         if count==0 :
            edges.append((i,len(nodes)-N+i))

      G=None
      if makenx :
         G=nx.Graph()
         for i,node in enumerate(nodes) :
            if node < (len(gates)+N):
               label="{0:d}".format(node)
            else :
               label="{0:d}'".format(node-len(gates)-N)
            G.add_node(int(node),label=label,pos=(float(coords[i][0]),float(coords[i][1])))

         for edge in edges :
            G.add_edge(int(edge[0]),int(edge[1]))
         self.G=G

      if verbose :
         print('Lattice created!',flush=True)

      self.gates=gates.copy()
      self.edges=edges.copy()
      self.nodes=nodes.copy()
      return self.G

def make_boundary_Hayden(boundarytype,N) :
   # Holographic duality from random tensor networks
   # https://arxiv.org/pdf/1601.01694.pdf
   if boundarytype=='0' :
      return [1]*N
   elif boundarytype=='A' :
      return [-1]*int(N/4) + [1]*int(3*N/4)
   elif boundarytype=='B' :
      return [ 1]*int(N/4) + [-1]*int(N/4) + [1]*int(2*N/4)
   elif boundarytype=='C' :
      return [ 1]*int(2*N/4) + [-1]*int(N/4) + [1]*int(1*N/4)
   elif boundarytype=='D' :
      return [ 1]*int(3*N/4) + [-1]*int(N/4)
   elif boundarytype=='BC' :
      return [ 1]*int(1*N/4) + [-1]*int(2*N/4) + [1]*int(1*N/4)
   elif boundarytype=='ABC' :
      return [-1]*int(3*N/4) + [1]*int(1*N/4)
   elif boundarytype=='AB' :
      return [-1]*int(N/2) + [1]*int(N/2)
   elif boundarytype=='AC' :
      return [-1]*int(N/4) + [1]*int(N/4) + [-1]*int(N/4) + [1]*int(N/4)
   else :
      print('boundary',boundarytype,'does not exist')
      sys.exit(0)

def get_neighbor_list(nodes,edges) :
   t1=np.array(list(map(lambda x : x[0],edges)),dtype=int)
   t2=np.array(list(map(lambda x : x[1],edges)),dtype=int)

   out={}
   for node in nodes :
      a=[i for i,x in enumerate(t1) if x==node]
      b=[i for i,x in enumerate(t2) if x==node]
      out[node]=list(set(t2[a].tolist().copy()+t1[b].tolist().copy()))
   return out 

def get_connected_positive_sites(neg_sites,neighbors,state) :
   out_neg_sites=[]
   out_connected_sites=[]
   for neg_site in neg_sites :
      is_boundary=False
      for connected_site in neighbors[neg_site] :
         if state[connected_site] > 0 :
            out_connected_sites.append(int(connected_site))
            is_boundary=True

      if (is_boundary) and (state[neg_site] < 0) :
         out_neg_sites.append(int(neg_site))
   return list(set(out_connected_sites)),list(set(out_neg_sites))

def Ising_monte_carlo(nodes, edges,inv_temp,efunc,
      state=[], boundary=[], numsteps=100, numsamples=1000, interval=5,
      coords=[], dx=1, dy=1, nxgraph=None,PT=[]) : 

   eff_size=len(nodes)
   if len(state) < 1 :
      state=list(np.random.choice([-1,1],size=eff_size))

   states = []
   Es = []
   if inv_temp=='inf' :
      neg_sites=list(set(np.arange(len(state),dtype=int)[state<0])) #no duplicating elements
      neighbors=get_neighbor_list(nodes,edges)

      for i in range(eff_size) :
         connected_sites,neg_sites=get_connected_positive_sites(neg_sites,neighbors,state)
         for site in connected_sites :
            flip_site=site

            E=efunc(edges,boundary,state)
            a=state[flip_site]
            state[flip_site]=int(-a)

            Edash=efunc(edges,boundary,state)
            dE=(Edash-E)
            if dE <= 0 :
               # keep it flipped
               neg_sites.append(int(flip_site))
               pass
            else :
               # back to the original state
               state[flip_site]=a
         neg_sites=list(set(neg_sites))
         E = efunc(edges,boundary,state)
         states += [state]
         Es += [E]
      
   else :
      for step in range(numsteps) :
         for site in range(eff_size) :
            flip_site=np.random.randint(0,eff_size) 
            E=efunc(edges,boundary,state)
            a=state[flip_site]
            state[flip_site]=int(-a)

            Edash=efunc(edges,boundary,state)
            dE=(Edash-E)
            if dE < 0 :
               # keep it flipped
               pass

            elif inv_temp == 'inf' : # this shouldn't happen
               # back to the original state
               state[flip_site]=a

            elif np.exp(-inv_temp*dE) < np.random.uniform() :
               # back to the original state
               state[flip_site]=a

      print(f'{numsteps=} done')
      print(f'now sampling {numsamples=} samples')

      for step in range(numsamples*interval) :
         flip_site=np.random.randint(0,eff_size) 
         E=efunc(edges,boundary,state)
         a=state[flip_site]
         state[flip_site]=int(-a)

         Edash=efunc(edges,boundary,state)
         dE=(Edash-E)
         if dE < 0 :
            # keep it flipped
            pass

         elif inv_temp == 'inf' : # this shouldn't happen
            # back to the original state
            state[flip_site]=a

         elif np.exp(-inv_temp*dE) < np.random.uniform() :
            # back to the original state
            state[flip_site]=a

         states += [state.copy()]
         Es += [efunc(edges,boundary,state)]

   return states,eff_size,Es

def save_nodes_and_edges(nodes,edges,fname) :

   fname_nodes=fname+'_nodes'
   fname_edges=fname+'_edges'


   np.savetxt(fname_nodes,np.array(nodes,dtype=int),fmt='%d')

   t1=list(map(lambda x : x[0],edges))
   t2=list(map(lambda x : x[1],edges))
   np.savetxt(fname_edges,[t1,t2],fmt='%d')

def load_nodes_and_edges(fname) :
   fname_nodes=fname+'_nodes'
   fname_edges=fname+'_edges'
   nodes=np.loadtxt(fname_nodes,dtype=int)
   edges=np.loadtxt(fname_edges,dtype=int)

   t1=edges[0,:]
   t2=edges[1,:]
   edges=list(map(lambda x,y : (x,y),t1,t2))
   return nodes,edges

def calc_energy_Hayden(edges,boundary,state) :
   # Holographic duality from random tensor networks
   # https://arxiv.org/pdf/1601.01694.pdf

   t1=list(map(lambda x : x[0],edges))
   t2=list(map(lambda x : x[1],edges))
   
   E_bulk = -sum(state[t1]*state[t2]-1)
   E_edge = -sum((state[-len(boundary):]*boundary - 1))
   return E_bulk + E_edge

def plot_heatmap(coords,state,dx,dy,step,obs) :
   from matplotlib import pyplot as plt 
   numgrid_per_spin=3

   xs=np.array(list(map(lambda x : x[1],coords)))
   ys=np.array(list(map(lambda x : x[0],coords)))

   xmax=max(xs)/dx+dx
   ymax=max(ys)/dy+dy

   dx_new=dx/numgrid_per_spin
   dy_new=dy/numgrid_per_spin
   
   xs=xs+dx/2+dx/100
   ys=ys+dy/2+dy/100

   hm = np.empty((int(xmax*numgrid_per_spin)+2,int(ymax*numgrid_per_spin)+2,))
   hm[:]=np.nan

   for spin in range(len(coords)) :
      x_start=int((xs[spin]-dx/2)/dx_new)
      y_start=int((ys[spin]-dy/2)/dy_new)
      for i in range(numgrid_per_spin) :
         for j in range(numgrid_per_spin) :
            if ((x_start+i) > 0) and ((y_start+i) > 0) :
               hm[x_start+i,y_start+j]=state[spin]

   ax=plt.gca()
   ttl = ax.text(0.5, 1.01, 'step={0:d}, {1}={2:.3f}'.format(step,list(obs.keys())[0],obs[list(obs.keys())[0]]),
         horizontalalignment='center', verticalalignment='bottom', transform=ax.transAxes)
   plot=ax.imshow(hm,vmin=-1,vmax=1,origin='lower',cmap='coolwarm',animated=True,aspect='auto') #,interpolation='none'
   return [ttl,plot]
