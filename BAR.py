# util
import sys,os
import time

# math
import numpy as np
from itertools import compress

# graph 
import networkx as nx

# plot
import matplotlib.pyplot as plt

# BAR 
import scipy.optimize

# Example of free energy difference calculation using 
# Barnett Acceptance Ratio (BAR) acceptance ratio.
# Bennett, C. H. (1976). Efficient estimation of free energy differences from Monte Carlo data. 
# Please cite: Journal of Computational Physics (Vol. 22, Issue 2, pp. 245–268). Elsevier BV. 
# https://doi.org/10.1016/0021-9991(76)90078-4.

def calc_dF(dE_0,dE_1,inv_temp) :
   barC=lambda x : np.mean(1./(1.+np.exp(inv_temp*(dE_0-x))))*(np.mean(1./(1+np.exp(inv_temp*(dE_1+x))))**-1.)
   min_square=lambda x: (barC(x) - 1.)**2.
   xopt = scipy.optimize.minimize(fun=min_square, x0=[1],tol=1e-10,options={'disp' :False})
   dF=xopt.x
   return dF

def bootstrap_BAR(dE_0,dE_1,inv_temp) :
   dF=calc_dF(dE_0,dE_1,inv_temp)

   numrep=1000
   samplesratio=0.5
   minsamples=100

   samples=0
   if minsamples < samplesratio*len(dE_0)  :
      samples=int(samplesratio*len(dE_0))
   elif len(dE_0) < minsamples :
      samples=len(dE_0)
   else :
      samples=int(minsamples)

   dFs=[]
   for i in range(numrep) :
      dE_0_curr=np.random.choice(dE_0, size=samples)
      dE_1_curr=np.random.choice(dE_1, size=samples)
      dFs.append(calc_dF(dE_0_curr,dE_1_curr,inv_temp))

   ddF=np.std(dFs)
   print(ddF)
   return dF,ddF

if __name__ == "__main__" :
   numlambda=10
   boundarytypes=['A','AB','AC']

   # stuff
   lambdas=np.linspace(0.,1.,numlambda,endpoint=False).tolist()+[1]
   lambdas=np.array(lambdas)

   print('N =',2**n,flush=True)
   inv_temp=1.

   numsamples={}
   Ents={}

   valid_stags=[]
   print(stag,boundarytype,flush=True)
   dFs=[]
   ddFs=[]
   for lambdaind in range(len(lambdas)-1) :
      simfname_dE='E_{0:.5f}.out_{1}'.format(lambdas[lambdaind],boundarytype)
      simfname_dE=simdir+simfname_dE
      if os.path.exists(simfname_dE) :
         [dE_0,dE_1]=np.loadtxt(simfname_dE,ndmin=2)

         if len(dE_0) < 2 :
            print(simfname_dE,flush=True)
            os.remove(simfname_dE)

         if maxnumsamples < len(dE_0)  :
            dE_0=dE_0[:maxnumsamples]
            dE_1=dE_1[:maxnumsamples]

         minnumsamples=min(len(dE_0),minnumsamples)

      dF,ddF=bootstrap_BAR(dE_0,dE_1,inv_temp)
      dFs.append(-dF)
      ddFs.append(ddF)


   if len(dFs) == numlambda :
      if not (boundarytype in Ents) :
         Ents[boundarytype]={}

      Ent=np.sum(dFs)
      dEnt=np.sqrt(np.sum(np.array(ddFs)**2.))
      print(Ent,dEnt)
      Ents[boundarytype][stag]=(Ent,dEnt)

      if  not (boundarytype in numsamples) :
         numsamples[boundarytype]={}
      
      numsamples[boundarytype][stag]=minnumsamples
      #outfname=outdir+'{}_{}_{}_{}_{}.out'.format(boundarytype,maxnumsamples,inv_temp_tag,stag,n)
      #print(outfname)
      #np.savetxt(outfname,[Ent,dEnt,minnumsamples])

   try :
      TMI=4*Ents['A'][stag][0]-2*Ents['AB'][stag][0]-Ents['AC'][stag][0]
      dTMI=np.qsrt( (4*Ents['A'][stag][1])**2.+(2*Ents['AB'][stag][0])**2.+(Ents['AC'][stag][0])**2.)
      print('beta={:.3f}'.format(inv_temp),'s={0:.3f}'.format(stag/10000))
      print('SA={0:.3f}'.format(Ents['A'][stag][0]))
      print('SAB={0:.3f}'.format(Ents['AB'][stag][0]))
      print('SAC={0:.3f}'.format(Ents['AC'][stag][0]))
      print('tmi={0:.3f}'.format(TMI),'pm',dTMI)
      valid_stags.append(stag)
   except :
      pass
