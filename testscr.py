# util
import sys,os

# math
import numpy as np

# graph 
import networkx as nx

# optimization
import scipy.optimize
from scipy.optimize import root_scalar

from IsingToolkit import *

def Ising_monte_carlo_2(nodes, edges,inv_temp,efunc,
      state0=[], state1=[], boundary0=[], boundary1=[], numsteps=100, numsamples=1000, interval=5,
      coords=[], tol=0.01) : 

   if len(state0) < 1 :
      state0=list(np.random.choice([-1,1],size=len(nodes)))
   if len(state1) < 1 :
      state1=list(np.random.choice([-1,1],size=len(nodes)))

   E0_0s = []
   E0_1s = []

   E1_1s = []
   E1_0s = []

   dFs = []

   def do_montecarlo_step(boundary,state) :
      flip_site = np.random.randint(0,len(nodes)) 
      E = efunc(edges,boundary,state)
      a = state[flip_site]
      state[flip_site] = int(-a)

      Edash=efunc(edges,boundary,state)
      dE=(Edash-E)
      if dE < 0 :
         # keep it flipped
         return Edash

      elif inv_temp == 'inf' : # this shouldn't happen
         # back to the original state
         state[flip_site]=a
         return E

      elif np.exp(-inv_temp*dE) < np.random.uniform() :
         # back to the original state
         state[flip_site]=a
         return E
      return Edash

   def analytical_error_bar(samples_A, samples_B):
      """Compute analytical error for BAR."""
      N_A = len(samples_A)
      N_B = len(samples_B)
      var_delta_F = (1 / (inv_temp**2.)) * (1 / N_A + 1 / N_B - 2 / (N_A + N_B))
      std_delta_F = np.sqrt(var_delta_F)
      return std_delta_F

   def bootstrap_error(dE_0, dE_1, num_bootstrap):
      """Compute free energy difference using bootstrap resampling."""
      delta_F_bootstrap = []

      for _ in range(num_bootstrap):
         # Resample with replacement
         _dE_0 = np.random.choice(dE_0, size=len(dE_0), replace=True)
         _dE_1 = np.random.choice(dE_1, size=len(dE_1), replace=True)

         bar_equation = lambda x : np.sum(1./(1.+np.exp(inv_temp*(_dE_0 - x)))) - np.sum(1./(1+np.exp(inv_temp*(_dE_1 + x))))
         result = root_scalar(bar_equation, bracket=[-10, 10])  # Adjust bracket as needed
         C = result.root
         dF = C - np.log(len(_dE_0) / len(_dE_1))
         delta_F_bootstrap += [dF]

      delta_F_mean = np.mean(delta_F_bootstrap)
      delta_F_std = np.std(delta_F_bootstrap)

      return delta_F_mean, delta_F_std

   for step in range(numsteps) :
      for site in range(len(nodes)) :
         do_montecarlo_step(boundary0,state0)
         do_montecarlo_step(boundary1,state1)

   print(f'{numsteps=} done')
   print(f'now sampling {numsamples=} samples')

   # BAR method for calculating free energy
   # Please cite
   # Bennett, C. H. (1976). Efficient estimation of free energy differences from Monte Carlo data. 
   # Journal of Computational Physics (Vol. 22, Issue 2, pp. 245–268). Elsevier BV. https://doi.org/10.1016/0021-9991(76)90078-4.

   for step in range(numsamples*interval) :
      E0_0 = do_montecarlo_step(boundary0,state0)
      E1_1 = do_montecarlo_step(boundary1,state1)

      E0_1 = efunc(edges,boundary1,state0)
      E1_0 = efunc(edges,boundary0,state1)

      if step % interval == 0 :
         E0_0s += [E0_0]
         E0_1s += [E0_1]
         E1_1s += [E1_1]
         E1_0s += [E1_0]

         if (step % (interval * 10000) == 0) and (step > 0) :
            dE_0=np.array(E0_1s) - np.array(E0_0s)
            dE_1=np.array(E1_0s) - np.array(E1_1s)

            bar_equation = lambda x : np.sum(1./(1.+np.exp(inv_temp*(dE_0 - x)))) - np.sum(1./(1+np.exp(inv_temp*(dE_1 + x))))
            result = root_scalar(bar_equation, bracket=[-10, 10])  # Adjust bracket as needed
            C = result.root
            dF = C - np.log(len(dE_0) / len(dE_1))

            num_bootstrap = 100
            _dF,ddF = bootstrap_error(dE_0, dE_1, num_bootstrap)
            print(len(E0_0s),"samples, dF =", dF,":", _dF, "\pm", ddF)
            if ddF < tol :
               print(f'tolerance {tol=:.5f} reached')
               return _dF, ddF

   dE_0=np.array(E0_1s) - np.array(E0_0s)
   dE_1=np.array(E1_0s) - np.array(E1_1s)

   num_bootstrap = 100
   _dF,ddF = bootstrap_error(dE_0, dE_1, num_bootstrap)

   print(f"exceeded {numsamples} samples. returning")
   return _dF,ddF


if __name__ == '__main__' :
   s=-100
   n=3 #log_2 
   N=2**n #number of qubits

   inv_temp = np.log(2.) # avoid beta as a variable name
   numsteps = 1000
   numsamples = 10000000
   interval = 5

   boundarytypes = ['A','B','C','AB','AC','BC','D']
   dFs = {}
   for boundarytype in boundarytypes : 
      print(boundarytype)
      boundary0=make_boundary_Hayden('0',N)
      boundary1=make_boundary_Hayden(boundarytype,N)

      dE_0=[]
      dE_1=[]

      # make lattice
      gates,probs=gen_all_gates_pwr2(s,n,tmax=1)
      curr_gates=get_random_lattice(gates,probs)
      lat=lattice()

      G=lat.make_lattice(N,curr_gates,makenx=True)

      coords=nx.get_node_attributes(G,'pos')
      coords=list(map(lambda x : (coords[x][0],coords[x][1]),coords))

      state0=np.random.choice([-1,1],size=len(G.nodes))
      state1=np.random.choice([-1,1],size=len(G.nodes))

      def generate_beta_range(beta_target, beta_min, beta_max, M):
         """Generate a geometric progression of inverse temperatures."""
         beta_list = beta_min * (beta_max / beta_min) ** (np.arange(M) / (M - 1))
         return beta_list

      inv_temp_list = generate_beta_range(inv_temp,0.5,1.5,10)

      dF,ddF = Ising_monte_carlo_2(G.nodes,G.edges,inv_temp,calc_energy_Hayden,
            state0=state0, state1=state1, boundary0=boundary0, boundary1=boundary1,
                                                 numsteps=numsteps, numsamples=numsamples, interval=interval)
      print(dF,ddF)
      dFs[boundarytype] = (dF,ddF)

   print(dFs['A'] + dFs['B'] - dFs['AB'] + dFs['A'] + dFs['C'] - dFs['AC'] - (dFs['A'] + dFs['BC'] - dFs['D']))
   #print('dF =',dF)
