# Ising Toolkit

Entanglement Entropy of a region of an output state of a Haar-random circuit is dual to the free energy different of an Ising model where region is pinned to -1 and rest is 1, 
and  everywhere is pinned as 1 (c.f. Hayden et al. (2016)).

This software has a feature of converting a circuit with given Haar-random circuit to a corresponding lattice model.
Then using Monte-Carlo solves classical Ising model on a lattice with non-trivial connectivity. 

I've given an example on calculating Renyi-2 Entropy from free energy difference computed with BAR method.

## Cite 
Please cite all of the following:

Example is based on 
* Hayden, P., Nezami, S., Qi, X.-L., Thomas, N., Walter, M., & Yang, Z. (2016). Holographic duality from random tensor networks.
In Journal of High Energy Physics (Vol. 2016, Issue 11). Springer Science and Business Media LLC. https://doi.org/10.1007/jhep11(2016)009.

BAR calculation is based on 
* Bennett, C. H. (1976). Efficient estimation of free energy differences from Monte Carlo data. 
In Journal of Computational Physics (Vol. 22, Issue 2, pp. 245–268). Elsevier BV. https://doi.org/10.1016/0021-9991(76)90078-4.

Cite this software as
* Tomohiro Hashizume. (2022) IsingToolkit 0.1.  https://doi.org/10.5281/zenodo.7245363

## TODO

Complete this read me...

## Usage

Executing `python IsingToolkit.py` will calculate free energy difference of sparsely coupled model. 
I think it's self explanatory.

## Contact

* Tomohiro Hashizume (original developer) tomohiro.hashizume.overseas@gmail.com

## License

MIT License

Copyright (c) [2022] [Tomohiro Hashizume]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Ongoing
